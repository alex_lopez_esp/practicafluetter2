import 'dart:ui';

import 'package:di_alex_lopez_p1/pages/product_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Product_Details_Page extends StatelessWidget {
  final Product product;

  const Product_Details_Page({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
        appBar: AppBar(
        title: const Text("Productos"),
        backgroundColor: Colors.deepPurpleAccent,
    ),

      body:Column(

        children: [

          Column(


            children: [

              Container(
                color: Colors.grey,
                  //decoration: const BoxDecoration(color: Colors.grey),
                padding:  EdgeInsets.all(20.0),

                child: SizedBox(

                  height: 200,width: 400,  child: Image.asset(product.image),

              )),
            ],

          ),
          Column(
            children: [
              Container(
                height: 100,width: 500,


                padding: EdgeInsets.only(left:15 ,top: 40),
                 decoration: const BoxDecoration(color: Colors.white,borderRadius: BorderRadius.only(topRight: Radius.circular(25),topLeft: Radius.circular(25))),
                child: Text(product.name,textAlign: TextAlign.left,style: const TextStyle(

                fontSize: 30.0,
                fontWeight: FontWeight.bold,
                  backgroundColor: Colors.white

                ),

                )

              )
            ],
          ),

          Column(

            children: [
              Container(
                  height: 100,width: 500,

                  decoration: BoxDecoration(color: Colors.white),
                  padding:  EdgeInsets.only(left: 15),
                  child: Text("€"+product.price.toString(),textAlign:TextAlign.left,style: const TextStyle(
                      backgroundColor: Colors.white,
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold

                  ),)
              ),

            ],
          ),
          Column(
            children: [
              Container(
                height: 163,width: 500,

                decoration: BoxDecoration(color: Colors.white),
                padding:  EdgeInsets.only(left: 15,right: 25),

                child: Text(product.description,style: const TextStyle(backgroundColor: Colors.white,fontSize: 14),),
              )
            ],
          )
        ],
      ),
    );
  }
}