import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'list_page.dart';

class Login_Page extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text(
                " Login",
                style: TextStyle(
                  fontSize: 40,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Times New Roman",
                ),
              ),
              const Spacer(),
              Container(
                width: 40,
                height: 90,
                decoration: const BoxDecoration(
                  color: Colors.deepPurpleAccent,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(1.5, 1.5),
                      color: Colors.black45,
                      blurRadius: 20,
                      spreadRadius: 4,
                    ),
                  ],
                ),
              ),
              Container(
                width: 70,
                height: 90,
                margin: const EdgeInsets.all(12),
                decoration: const BoxDecoration(
                    color: Colors.deepPurpleAccent,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(1.5, 1.5),
                        color: Colors.black45,
                        blurRadius: 20,
                        spreadRadius: 4,
                      )
                    ]),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(

                child: Container(

                   child: Container(
                     child: Column(
                       children:const <Widget> [
                         Spacer(),
                        TextField(
                         decoration: InputDecoration(
                           prefixIcon: Icon(
                             Icons.account_circle,
                             color: Colors.deepPurpleAccent,
                             size: 20,



                           ),

                           labelText: "Username",
                            contentPadding: EdgeInsets.only(left: 5),
                           ),
                         ),


                        TextField(

                          decoration: InputDecoration(

                              prefixIcon: Icon(
                          Icons.vpn_key,
                          color: Colors.deepPurpleAccent,
                          size: 20,
                        ),
                        labelText: "Password"
                        ),
                        ),
                         Spacer(),

                       ],
                     ),


                    margin: const EdgeInsets.only(),
                    padding: const EdgeInsets.all(25),

                  ),

                    width: 350,
                    height: 250,
                    margin: const EdgeInsets.only(top: 100),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius:BorderRadius.all(Radius.circular(25)),
                      boxShadow: [

                        BoxShadow(
                          offset: Offset(1.5, 1.5),
                          color: Colors.black45,
                          blurRadius: 20,
                          spreadRadius: 4,

                        ),
                      ],
                    ),

              ),


              ),

            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,

            children: [
              Container(


                margin: const EdgeInsets.only(right: 10,top: 100),
                child: TextButton(

                  child: const Text("LOGIN",style: TextStyle(color: Colors.white),),
                  style: TextButton.styleFrom(backgroundColor: Colors.deepPurpleAccent ,
                    padding: const EdgeInsets.only(left: 50,right: 50),
                    shape: const RoundedRectangleBorder(
                      borderRadius:BorderRadius.only(topLeft: Radius.circular(25),bottomLeft:Radius.circular(25)),

                    ),


                 ) ,

                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return  List_Page();
                    }
                    ));


                  },


                ),
              ),
            ],

          ),
        ],
      ),
      backgroundColor: Colors.grey[200],
    );
  }
}
