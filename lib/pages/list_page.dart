import 'dart:convert';

import 'package:di_alex_lopez_p1/pages/product_data.dart';
import 'package:di_alex_lopez_p1/pages/product_details_page.dart';
import 'package:flutter/material.dart';

class List_Page extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Productos"),
        backgroundColor: Colors.deepPurpleAccent,
      ),
      body: Center(

      child: FutureBuilder(

        future: DefaultAssetBundle.of(context).loadString(
            "assets/json/products.json"),
        builder: (context, AsyncSnapshot<String>snapshot) {
          if (snapshot.hasData) {
            Map<String, dynamic>data = json.decode(snapshot.data);
            var products = Products.fromJson(data);

            return ListView.separated(


                itemCount: products.products.length,
                itemBuilder: (context,index) => ListTile(

                  title:Text(products.products[index].name),
                  leading:SizedBox(height: 100,width: 100,child: Image.asset(products.products[index].image),) ,

                  subtitle: Text(products.products[index].price.toString()+"€"),
                  trailing: Icon(Icons.arrow_forward_ios),
                  onTap:(){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context)=>Product_Details_Page(product: products.products[index]),
                    ),
                    );
                  },
                ),
              
              separatorBuilder: (_,index)=>Divider(
                thickness: 0.2,
                color: Colors.cyanAccent,
              ),


            );
          } else {
            return CircularProgressIndicator();
          }


        }

      ),

    ),
      backgroundColor: Colors.white,

    );
  }
}




  
